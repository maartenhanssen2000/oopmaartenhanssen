﻿using System;
using System.Collections.Generic;

namespace SchoolAdmin
{
    public abstract class Person
    {
        public Person(string name,string lastName, DateTime birthday, int schoolId, int id, string contactNumber)
        {
            Name = name;
            LastName = lastName;
            Birthday = birthday;
            SchoolId = schoolId;
            Id = id;
            ContactNumber = contactNumber;
        }

        public string Name;
        public string LastName;
        public DateTime Birthday;
        public int SchoolId;
        public int Id;
        protected string ContactNumber;


        public abstract string ShowOne();

        public virtual string GetNameTagText()
        {
            return $"{Name} {LastName}";
        }
    }
}