﻿using System;
using System.Collections.Generic;

namespace SchoolAdmin
{
    public class Student : Person
    {
        private static List<Student> List = new List<Student>();

        public Student(string name,string lastName, DateTime birthday, int schoolId, int id, string contactNumber):base(name, lastName, birthday, schoolId,id, contactNumber)
        {
            List.Add(this);
        }


        public override string ShowOne()
        {
            return $"Gegevens van de student {Name}, {LastName}, {Birthday}, {SchoolId}, {SchoolId}";
        }

        public static string ShowAll()
        {
            string text = "Lijst van studenten:\n";
            foreach (var student in List)
            {
                text += $"{student.Name}, {student.LastName}, {student.Birthday}, {student.SchoolId}, {student.Id}\n";
            }
            return text;
        }
    }
}