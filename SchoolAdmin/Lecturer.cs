﻿using System;
using System.Collections.Generic;

namespace SchoolAdmin
{
    public class Lecturer : Person
    {
        private static List<Lecturer> List = new List<Lecturer>();
        public List<Course> Courses;

        public Lecturer(string name,string lastName, DateTime birthday, int schoolId, int id, string contactNumber):base(name,lastName,birthday,schoolId,id, contactNumber)
        {
           List.Add(this);
           Courses = new List<Course>();
        }


        public override string ShowOne()
        {
            return $"Gegevens van de lector {Name}, {LastName}, {Birthday}, {SchoolId}, {SchoolId}";
        }

        public static string ShowAll()
        {
            string text = "Lijst van lectoren:\n";
            foreach (var lecturer in List)
            {
                text += $"{lecturer.Name}, {lecturer.LastName}, {lecturer.Birthday}, {lecturer.SchoolId}, {lecturer.Id}\n";
            }
            return text;
        }

        public string ShowTaughtCourses()
        {
            string result =  "Vakken van de lector:\n";
            foreach (var course in Courses)
            {
                result += $"- {course.Title} ({course.CalculateWorkload()})\n";
            }
            return result;
        }
        
        public override string GetNameTagText()
        {
            return $"(LECTOR) {Name} {LastName}\n{ContactNumber}";
        }
    }
}