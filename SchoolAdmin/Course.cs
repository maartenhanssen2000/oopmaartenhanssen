﻿using System.Reflection;

namespace SchoolAdmin
{
    public abstract class Course
    {
        public string Title;

        public Course(string title)
        {
            Title = title;
        }

        public abstract uint CalculateWorkload();
    }
}