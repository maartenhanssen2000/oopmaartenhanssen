﻿using System.Collections.Generic;

namespace SchoolAdmin
{ 
    class School
    {
        public School(string name,string street, string postalCode, string city, int id)
        {
            Name = name;
            Street = street;
            PostalCode = postalCode;
            City = city;
            Id = id;
            List.Add(this);
        }

        private string Name;
        private string Street;
        private string PostalCode;
        private string City;
        private int Id;
        private static List<School> List = new List<School>();


        public static string ShowAll()
        {
            string text = "Lijst van scholen:\n";
            foreach (var school in List)
            {
                text += $"{school.Name}, {school.Street}, {school.City}, {school.Id}\n";
            }
            
            return text;
        }

        public string showOne()
        {
            return $"Gegevens van de school {Name}, {Street}, {City}, {Id}";
        }
    }
}