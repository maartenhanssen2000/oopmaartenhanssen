﻿using System;

namespace SchoolAdmin
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Schoolbeheer");
            School bsDeSpits = new School("GO! BS de spits", "Thonetlaan 106", "2050", "Antwerpen", 1);
            School atheneumDeurne = new School("GO! Koniklijk Atheneum Deurne", "Fr. Craeybeckxlaan 22", "2100", "Deurne", 2);
            Student mohamed = new Student("Mohamed","El Farisi",new DateTime(1987-12-06),1,1, "");
            Student sarah = new Student("sarah","Jansens",new DateTime(1991-10-21),2,2, "");
            Student bart = new Student("bart","Janssens",new DateTime(1990-10-21),2,3, "");
            Student farah = new Student("Farah","El Farisi",new DateTime(1987-12-06),1,4, "");
            Lecturer adem = new Lecturer("Adem", "Kaya", new DateTime(1976-12-01), 1, 1, "123456789");
            Lecturer anne = new Lecturer("Anne", "Wouters", new DateTime(1968-04-03), 2, 2, "123456789");
            AdministrativeStaff raul = new AdministrativeStaff("Raul", "Jacob", new DateTime(1985-11-01), 1, 1, "123456789");
            Course theoryCourse = new TheoryCourse("OO Programmeren", 3);
            Course seminar = new Seminar("Docker");
            anne.Courses.Add(theoryCourse);
            anne.Courses.Add(seminar);
            Console.WriteLine(School.ShowAll());
            Console.WriteLine(Student.ShowAll());
            Console.WriteLine(Lecturer.ShowAll());
            Console.WriteLine(AdministrativeStaff.ShowAll());
            Console.WriteLine(farah.GetNameTagText());
            Console.WriteLine(adem.GetNameTagText());
            Console.WriteLine(raul.GetNameTagText());
            Console.WriteLine(anne.ShowTaughtCourses());
        }
    }
}