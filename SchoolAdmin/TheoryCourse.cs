﻿using System.Net.Http.Headers;

namespace SchoolAdmin
{
    public class TheoryCourse: Course
    {
        private byte studyPoints;

        public TheoryCourse(string title, byte studyPoints) : base(title)
        {
            this.studyPoints = studyPoints;
        }

        public override uint CalculateWorkload()
        {
            return studyPoints * (uint) 4;
        }
    }
}