﻿using System;
using System.Collections.Generic;

namespace SchoolAdmin
{
    public class AdministrativeStaff : Person
    {
        private static List<AdministrativeStaff> List = new List<AdministrativeStaff>();

        public AdministrativeStaff(string name,string lastName, DateTime birthday, int schoolId, int id, string contactNumber):base(name, lastName, birthday, schoolId,id, contactNumber)
        {
            List.Add(this);
        }


        public override string ShowOne()
        {
            return $"Gegevens van de student {Name}, {LastName}, {Birthday}, {SchoolId}, {SchoolId}";
        }

        public static string ShowAll()
        {
            string text = "Lijst van administratie:\n";
            foreach (var student in List)
            {
                text += $"{student.Name}, {student.LastName}, {student.Birthday}, {student.SchoolId}, {student.Id}\n";
            }
            return text;
        }
        
        public override string GetNameTagText()
        {
            return $"(ADMINISTRATIE) {Name} {LastName}\n {ContactNumber}";
        }
    }
}