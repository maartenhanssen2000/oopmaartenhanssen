﻿using System.Net.Http.Headers;

namespace SchoolAdmin
{
    public class Seminar: Course
    {
        public Seminar(string title) : base(title)
        {
            
        }

        public override uint CalculateWorkload()
        {
            return 20;
        }
    }
}