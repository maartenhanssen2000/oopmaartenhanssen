﻿using System.Net.Http.Headers;

namespace SchoolAdmin
{
    public class LabCourse: Course
    {
        private byte StudyPoints;
        private string Materials;

        public LabCourse(string title, byte studyPoints, string materials) : base(title)
        {
            StudyPoints = studyPoints;
            Materials = materials;
        }

        public override uint CalculateWorkload()
        {
            return StudyPoints * (uint) 2;
        }
    }
}