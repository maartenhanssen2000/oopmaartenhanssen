﻿using System;

namespace OOP
{
    public class ResultV1
    {
        public static void Main()
        {
            static void PrintHonors(int result)
            {
                if (result < 50)
                {
                    Console.WriteLine("Niet geslaag");
                }else if (result < 68)
                {
                    Console.WriteLine("Voldoende");
                }else if (result < 75)
                {
                    Console.WriteLine("Onderscheiding");
                }else if (result < 85)
                {
                    Console.WriteLine("Grote onderscheiding");
                }
                else
                {
                    Console.WriteLine("Grootste onderscheiding");
                }
            }
            PrintHonors(75);
        }
    }
}