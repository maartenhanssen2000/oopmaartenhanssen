﻿using System;

namespace OOP
{
    public class NumberCombination
    {
        public int Number1
        {
            get;
            set;
        }

        public int Number2
        {
            get;
            set;
            
        }
        
        public  double Sum()
        {
            double result = Convert.ToDouble(Number1 + Number2);
            return result;
        }
        public  double Sub()
        {
            double result = Convert.ToDouble(Number1 - Number2);
            return result;
        }
        public  double Mult()
        {
            double result = Convert.ToDouble(Number1 * Number2);
            return result;
        }
        public  string Div()
        {
            string result;
            if (Number1 == 0 || Number2 == 0)
            {
                result = Convert.ToString(Number1 + Number2);
            }
            else
            {
                result = "Error";
            }
            
            return result;
        }
    }
}