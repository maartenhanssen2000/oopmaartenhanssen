﻿using System;

namespace OOP {
    class Soldier {
        public int Health;
        public int Damage;

        int Hit()
        {
            Random random = new Random();
            int hit = random.Next(0, Damage);
            return hit;
        }
    }
}