﻿using System;

namespace OOP
{
    public class H8
    {
        public static void Main()
        {
            while (true)
            {
                Console.WriteLine("Submenu Hoofdstuk 8");
                Console.WriteLine("1. H8-RapportModule-V1\n2. H8-RapportModule-V2\n3. H8-Getallencominatie\n4. H8-Figuren\n5. H8-Studentklasse" +
                                  "\n6. H8-honden\n7. H8-uniform-soldiers\n8. H8-MathProgram\n9. Main menu");

                bool succes = Int32.TryParse(Console.ReadLine(), out int choice);
                if (succes)
                {
                    switch (choice)
                    {
                        case 1:
                            Console.WriteLine($"Je hebt gekozen voor {choice}.");
                            System.Threading.Thread.Sleep(1000);
                            Console.Clear();
                            ResultV1.Main();
                            break;
                        case 2:
                            Console.WriteLine($"Je hebt gekozen voor {choice}.");
                            System.Threading.Thread.Sleep(1000);
                            Console.Clear();
                            ResultV2.Main();
                            break;
                        case 3:
                            Console.WriteLine($"Je hebt gekozen voor {choice}.");
                            System.Threading.Thread.Sleep(1000);
                            Console.Clear();
                            NumberCominator();
                            break;
                        case 4:
                            Console.WriteLine($"Je hebt gekozen voor {choice}.");
                            System.Threading.Thread.Sleep(1000);
                            Console.Clear();
                            FigureProgram.Main();
                            break;
                        case 5:
                            Console.WriteLine($"Je hebt gekozen voor {choice}.");
                            System.Threading.Thread.Sleep(1000);
                            Console.Clear();
                            Student.Main();
                            break;
                        case 6:
                            Console.WriteLine($"Je hebt gekozen voor {choice}.");
                            System.Threading.Thread.Sleep(1000);
                            Console.Clear();
                            BarkingProgram.Main();
                            break;
                        case 7:
                            Console.WriteLine($"Je hebt gekozen voor {choice}.");
                            System.Threading.Thread.Sleep(1000);
                            Console.Clear();
                            SoldierGame.Main();
                            break;
                        case 8:
                            Console.WriteLine($"Je hebt gekozen voor {choice}.");
                            System.Threading.Thread.Sleep(1000);
                            Console.Clear();
                            MathProgram.Main();
                            break;
                        case 9:
                            Console.WriteLine($"Je hebt gekozen voor {choice}.");
                            System.Threading.Thread.Sleep(1000);
                            Console.Clear();
                            Program.Main();
                            break;
                        default:
                            Console.Clear();
                            Console.WriteLine("Ongeldige keuze. ");
                            System.Threading.Thread.Sleep(1000);
                            Console.Clear();
                            break;
                    }
                } else
                {
                    Console.Clear();
                    Console.WriteLine("Geef een getal in.");
                    System.Threading.Thread.Sleep(1000);
                    Console.Clear();
                }
            }
            
            static void NumberCominator()
            {
                NumberCombination pair1 = new NumberCombination();
                pair1.Number1 = 12;
                pair1.Number2 = 34;
                Console.WriteLine("Paar:" + pair1.Number1 + ", " + pair1.Number2);
                Console.WriteLine("Sum = " + pair1.Sum());
                Console.WriteLine("Verschil = " + pair1.Sub());
                Console.WriteLine("Product = " + pair1.Mult());
                Console.WriteLine("Quotient = " + pair1.Div());  
            }
        }
    }
}