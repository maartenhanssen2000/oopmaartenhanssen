﻿using System;
using System.Collections.Generic;
using System.Linq;
namespace OOP
{
    public class Student
    {
        public string Name
        {
            get;
            set;
        }

        public int Age
        {
            get;
            set;
        }

        public int MarkCommunication
        {
            get;
            set;
        }

        public int MarkProgrammingPrinciples
        {
            get;
            set;
        }

        public int MarkWebTech
        {
            get;
            set;
        }

        double ComputeOverallMark()
        {
            double overall = Convert.ToDouble((MarkCommunication + MarkProgrammingPrinciples + MarkWebTech) / 3);
            overall = Math.Round(overall, 1);
            return overall;
        }

        public virtual void ShowOverview()
        {
            Console.WriteLine($"{Name}, {Age} jaar\nklass: \n\nCijferraport:\n********\nCommunicatie:\t\t{MarkCommunication}" +
                              $"\nProgramming Principles:\t{MarkProgrammingPrinciples}\nWeb Technology:\t\t{MarkWebTech}\nGemiddelde:\t\t{ComputeOverallMark()}");
        }

        public Student()
        {
            
        }

        public Student(string name = "onbekend", int age = 18, int markCommunication = 10, int markProgrammingPrinciples = 10, int markWebTech = 10)
        {
            Name = name;
            Age = age;
            MarkCommunication = markCommunication;
            MarkProgrammingPrinciples = markProgrammingPrinciples;
            MarkWebTech = markWebTech;
        }
        
        public static void ExecuteStudentMenu()
        {
            List<Student> studentsList = new List<Student>();
            bool loop = true;
            while (loop == true)
            {
                Console.WriteLine(
                    "Kies uit de volgende opties:\n1. gegevens van de studenten tonen\n2. een nieuwe student toevoegen\n3. gegevens van een bepaalde student aanpassen\n4. een student uit het systeem verwijderen\n5. stoppen");
                int input = Convert.ToInt32(Console.ReadLine());
                switch (input)
                {
                    case 1:
                        Console.Clear();
                        foreach (var student in studentsList)
                        {
                            student.ShowOverview();
                        }
                        break;
                    case 2:
                        Console.Clear();
                        Console.Write("Naam: ");
                        string name = Console.ReadLine();
                        Console.Write("Leeftijd: ");
                        int age = Convert.ToInt32(Console.ReadLine());
                        Console.Write("Cijfer communicatie: ");
                        int markCommunication = Convert.ToInt32(Console.ReadLine());
                        Console.Write("cijfer programmeren: ");
                        int markProgramming = Convert.ToInt32(Console.ReadLine());
                        Console.Write("cijfer webtechnologie: ");
                        int markWebTech = Convert.ToInt32(Console.ReadLine());
                        studentsList.Add(new Student(name, age, markCommunication, markProgramming, markWebTech));
                        break;
                    case 3:
                        Console.Clear();
                        Console.WriteLine("Wat is de indexpositie van de student die je wilt aanpassen?");
                        int index = Convert.ToInt32(Console.ReadLine());
                        if (studentsList.ElementAtOrDefault(index) != null)
                        {
                            Console.WriteLine("wat wilt u aanpassen\n1. Naam\n2. Leeftijd\n3. Cijfer communicatie\n4. Cijfer programmeren\n5. Cijfer webtechnologie");
                            int aanpassing = Convert.ToInt32(Console.ReadLine());
                            switch (aanpassing)
                            {
                                case 1:
                                    studentsList[index].Name = Console.ReadLine();
                                    break;
                                case 2:
                                    studentsList[index].Age = Convert.ToInt32(Console.ReadLine());
                                    break;
                                case 3:
                                    studentsList[index].MarkCommunication = Convert.ToInt32(Console.ReadLine());
                                    break;
                                case 4:
                                    studentsList[index].MarkProgrammingPrinciples = Convert.ToInt32(Console.ReadLine());
                                    break;
                                case 5:
                                    studentsList[index].MarkWebTech = Convert.ToInt32(Console.ReadLine());
                                    break;
                                default:
                                    Console.WriteLine("een juiste nummer in ");
                                    break;
                            }
                        }
                        else
                        {
                            Console.WriteLine("Geef een geldige index in");
                        }

                        break;
                    case 4:Console.Clear();
                        Console.WriteLine("Wat is de indexpositie van de student die je wilt verwijderen?");
                        index = Convert.ToInt32(Console.ReadLine());
                        if (studentsList.ElementAtOrDefault(index) != null)
                        {
                          studentsList.RemoveAt(index);  
                        }
                        else
                        {
                            Console.WriteLine("Geef een geldige index in");
                        }
                        
                        break;
                    case 5:
                        loop = false;
                        break;
                    default:
                        Console.Clear();
                        Console.WriteLine("Gelieve een juiste keuze te maken");
                        break;
                }
            }
        }

        public static void Main()
        {
            Student student1= new Student();
            //student1.Class = Classes.EA2;
            student1.Age = 21;
            student1.Name = "Joske Vermeulen";
            student1.MarkCommunication = 12;
            student1.MarkProgrammingPrinciples = 15;
            student1.MarkWebTech = 13;
            student1.ShowOverview(); 
        }
    }
    
}