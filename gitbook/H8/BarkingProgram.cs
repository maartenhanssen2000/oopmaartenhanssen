﻿using System;

namespace OOP {
    
    class BarkingDog {
        public string Name;
        public string Breed;

        public string Bark() {
                if(Breed == "German Shepherd") {
                    return "RUFF!";
                }
                else if(Breed == "Wolfspitz") {
                    return "AwawaWAF!";
                }
                else if(Breed == "Chihuahua") {
                    return "ARF ARF ARF!";
                }
                // dit zou nooit mogen gebeuren
                // maar als de programmeur van Main iets fout doet, kan het wel
                else {
                    return "Euhhh... Miauw?";
                }
            }
    }
    
    class BarkingProgram {

        // nu maken we onze randomgenerator *buiten* Main
        public static Random rng = new Random();
        public static void Main() {
            BarkingDog dog1 = new BarkingDog();
            BarkingDog dog2 = new BarkingDog();
            dog1.Name = "Swieber";
            dog2.Name = "Misty";
            int dog1BreedNumber = rng.Next(0,3);
            int dog2BreedNumber = rng.Next(0,3);
            if(dog1BreedNumber == 0) {
                dog1.Breed = "German Shepherd";
            }
            else if(dog1BreedNumber == 1) {
                dog1.Breed = "Wolfspitz";
            }
            else {
                dog1.Breed = "Chihuahua";
            }
            if(dog2BreedNumber == 0) {
                dog2.Breed = "German Shepherd";
            }
            else if(dog2BreedNumber == 1) {
                dog2.Breed = "Wolfspitz";
            }
            else {
                dog2.Breed = "Chihuahua";
            }
            while(true) {
                Console.WriteLine(dog1.Bark());
                Console.WriteLine(dog2.Bark());
            }
        }
    }
    
}