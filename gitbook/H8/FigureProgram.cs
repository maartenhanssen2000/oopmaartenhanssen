﻿using System;

namespace OOP
{
    public class FigureProgram
    {
        public static void Main()
        {
            Rectangle rectangle1 = new Rectangle();
            rectangle1.Height = 1.5;
            rectangle1.Width = 2.2;
            Rectangle rectangle2 = new Rectangle();
            rectangle2.Height = 1;
            rectangle2.Width = 3;
            Triangle triangle1 = new Triangle();
            triangle1.Base = 3;
            triangle1.Height = 1;
            Triangle triangle2 = new Triangle();
            triangle2.Base = 3;
            triangle2.Height = 1;
            Console.WriteLine($"een rechthoek met een breedte van {rectangle1.Width}m en een hoogte van {rectangle1.Height}m heeft een oppervalkte van {rectangle1.ComputeSurface()}");
            Console.WriteLine($"een rechthoek met een breedte van {rectangle2.Width}m en een hoogte van {rectangle2.Height}m heeft een oppervalkte van {rectangle2.ComputeSurface()}");
            Console.WriteLine($"een driehoek met een basis van {triangle1.Base}m en een hoogte van {triangle1.Height}m heeft een oppervalkte van {triangle1.ComputeSurface()}");
            Console.WriteLine($"een driehoek met een basis van {triangle2.Base}m en een hoogte van {triangle2.Height}m heeft een oppervalkte van {triangle2.ComputeSurface()}");
        }
    }
}