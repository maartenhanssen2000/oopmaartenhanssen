﻿namespace OOP
{
    public class Triangle
    {
        public int Base
        {
            get;
            set;
        }

        public int Height
        {
            get;
            set;
        }
        
        public double ComputeSurface()
        {
            return Base * Height / 2 ;
        }
    }
}