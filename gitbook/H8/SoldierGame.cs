﻿using System;

namespace OOP {
    class SoldierGame {
        public static void Main() {
            Soldier soldier1 = new Soldier();
            soldier1.Health = 100;
            soldier1.Damage = 20;
            Soldier soldier2 = new Soldier();
            soldier2.Health = 99; // om maar te tonen dat dit mag verschillen per soldaat
            soldier2.Damage = 20;
            Soldier soldier3 = new Soldier();
            soldier3.Health = 98;
            soldier3.Damage = 20;
            // beeld je in dat de game wat verder loopt
            // nu volgt de upgrade
            soldier1.Damage *= 2;
            soldier2.Damage *= 2;
            soldier3.Damage *= 2;
        }
    }
}