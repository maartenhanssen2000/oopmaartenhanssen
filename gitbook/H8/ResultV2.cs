﻿namespace OOP
{
    enum Honors
    {
        nietGeslaagd, voldoende, onderscheiding, groteOnderscheiding, grootsteOnderscheiding
    }
    public class ResultV2
    {
        static OOP.Honors ComputeHonors(int result)
        {
            if (result < 50)
            {
                return Honors.nietGeslaagd;
            }else if (result < 68)
            {
                return Honors.voldoende;
            }else if (result < 75)
            {
                return Honors.onderscheiding;
            }else if (result < 85)
            {
                return Honors.groteOnderscheiding;
            }
            else
            {
                return Honors.grootsteOnderscheiding;
            } 
        }

        public static void Main()
        {
            
        }
    }
}