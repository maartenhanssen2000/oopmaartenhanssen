﻿namespace OOP
{
    public class Rectangle
    {
        public double Width
        {
            get;
            set;
        }

        public double Height
        {
            get;
            set;
        }

        public double ComputeSurface()
        {
            return Width * Height;
        }
        
    }
}