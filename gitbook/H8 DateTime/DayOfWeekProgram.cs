﻿using System;

namespace OOP
{
    public class DayOfWeekProgram
    {
        public static void Main()
        {
            Console.WriteLine("Welke dag?");
            int day = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Welke maand?");
            int month = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Welk jaar?");
            int year = Int32.Parse(Console.ReadLine());
            
            DateTime date = new DateTime(year, month, day);
            Console.WriteLine($"{date.ToString("dd MMMM yyyy")} is een {date.DayOfWeek}");
        }
    }
}