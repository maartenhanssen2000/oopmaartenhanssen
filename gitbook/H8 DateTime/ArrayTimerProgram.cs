﻿using System;

namespace OOP
{
    public class ArrayTimerProgram
    {
        public static void Main()
        {
            DateTime start = DateTime.Now;
            int[] intArray = new int[1000000];
            for (int i = 0; i < intArray.Length; i++)
            {
                intArray[i] = i;
            }
            DateTime end = DateTime.Now;
            TimeSpan timer = end - start;
            Console.WriteLine($"Het duurt {timer.Milliseconds} milliseconden om een array van een miljoen elementen aan te maken en op te vullen met opeenvolgende waarden.");
        }
    }
}