﻿using System;

namespace OOP
{
    public class LeapYearProgram
    {
        public static void Main()
        { 
            int counter = 0;
            for (int i = 1800; i <= 2020; i++)
            {
                
                DateTime year = new DateTime(i,1,1);
                if (DateTime.IsLeapYear(year.Year)==true)
                {
                    counter++;
                }
            }

            Console.WriteLine($"Er zijn {counter} schrikkeljaren tussen 1800 en 2020");
        }
    }
}