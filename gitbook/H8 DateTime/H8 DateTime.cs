﻿using System;

namespace OOP
{
    public class H8_DateTime
    {
        public static void Main()
        {
            while (true)
            {
                Console.WriteLine("Submenu Hoofdstuk 8 DateTime");
                Console.WriteLine("1. h8-dag-van-de-week\n2. H8-ticks-sinds-2000\n3. H8-schrikkelteller" +
                                  "\n4. H8-simple-timing\n5. Main menu");

                bool succes = Int32.TryParse(Console.ReadLine(), out int choice);
                if (succes)
                {
                    switch (choice)
                    {
                        case 1:
                            Console.WriteLine($"Je hebt gekozen voor {choice}.");
                            System.Threading.Thread.Sleep(1000);
                            Console.Clear();
                            DayOfWeekProgram.Main();
                            break;
                        case 2:
                            Console.WriteLine($"Je hebt gekozen voor {choice}.");
                            System.Threading.Thread.Sleep(1000);
                            Console.Clear();
                            Ticks2000Program.Main();
                            break;
                        case 3:
                            Console.WriteLine($"Je hebt gekozen voor {choice}.");
                            System.Threading.Thread.Sleep(1000);
                            Console.Clear();
                            LeapYearProgram.Main();
                            break;
                        case 4:
                            Console.WriteLine($"Je hebt gekozen voor {choice}.");
                            System.Threading.Thread.Sleep(1000);
                            Console.Clear();
                            ArrayTimerProgram.Main();
                            break;
                        case 5:
                            Console.WriteLine($"Je hebt gekozen voor {choice}.");
                            System.Threading.Thread.Sleep(1000);
                            Console.Clear();
                            Program.Main();
                            break;
                        default:
                            Console.Clear();
                            Console.WriteLine("Ongeldige keuze. ");
                            System.Threading.Thread.Sleep(1000);
                            Console.Clear();
                            break;
                    }
                } else
                {
                    Console.Clear();
                    Console.WriteLine("Geef een getal in.");
                    System.Threading.Thread.Sleep(1000);
                    Console.Clear();
                }
            }
        }
    }
}