﻿using System;

namespace OOP
{
    public class Ticks2000Program
    {
        public static void Main()
        {
            DateTime now = DateTime.Now;
            DateTime year2000 = new DateTime(2000,1,1);

            TimeSpan timePassed = now - year2000;

            Console.WriteLine($"Sinds 1 januarie 2000 zijn er {timePassed.Ticks} ticks voorbijgegaan.");
        }
    }
}