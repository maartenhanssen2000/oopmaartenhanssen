﻿using System;
using OOProgrammeren;

namespace OOP
{
    public class H9
    {
        public static void Main()
        {
            while (true)
            {
                Console.WriteLine("Submenu Hoofdstuk 9");
                Console.WriteLine("1. H9-pokeattack\n2. H9-consciouspokemon\n3. h9-consciouspokemon-improved\n4. H9-pokevalueref\n5. Main menu");

                bool succes = Int32.TryParse(Console.ReadLine(), out int choice);
                if (succes)
                {
                    switch (choice)
                    {
                        case 1:
                            Console.WriteLine($"Je hebt gekozen voor {choice}.");
                            System.Threading.Thread.Sleep(1000);
                            Console.Clear();
                            Pokemon.MakePokemon();
                            break;
                        case 2:
                            Console.WriteLine($"Je hebt gekozen voor {choice}.");
                            System.Threading.Thread.Sleep(1000);
                            Console.Clear();
                            Pokemon.TestConsciousPokemon();
                            break;
                        case 3:
                            Console.WriteLine($"Je hebt gekozen voor {choice}.");
                            System.Threading.Thread.Sleep(1000);
                            Console.Clear();
                            Pokemon.TestConsciousPokemonSafe();
                            break;
                        case 4:
                            Console.WriteLine($"Je hebt gekozen voor {choice}.");
                            System.Threading.Thread.Sleep(1000);
                            Console.Clear();
                            Pokemon.DemoRestoreHP();
                            break;
                        case 5:
                            Console.WriteLine($"Je hebt gekozen voor {choice}.");
                            System.Threading.Thread.Sleep(1000);
                            Console.Clear();
                            Program.Main();
                            break;
                        default:
                            Console.Clear();
                            Console.WriteLine("Ongeldige keuze. ");
                            System.Threading.Thread.Sleep(1000);
                            Console.Clear();
                            break;
                    }
                } else
                {
                    Console.Clear();
                    Console.WriteLine("Geef een getal in.");
                    System.Threading.Thread.Sleep(1000);
                    Console.Clear();
                }
            }
        }
    }
}