﻿namespace OOP
{
    public enum PokeSpecies
    {
        Bulbasaur,
        Charmander,
        Squirtle,
        Pikachu
    }
}