﻿using System;

namespace OOP
{
    public class Pokemon
    {
        private int maxHP = 20;
        public int MaxHP
        {
            get
            {
                return maxHP;
            }
            set
            {
                if (value >= 20 && value <= 1000)
                    maxHP = value;
                else if (value < 20)
                    maxHP = 20;
                else
                    maxHP = 1000;
            }
        }
        private int hp = 20;
        public int HP
        {
            get
            {
                return hp;
            }
            set
            {
                if (value >= 0 && value <= MaxHP)
                    hp = value;
                else if (value < 0)
                    hp = 0;
                else
                    hp = MaxHP;
                
            }
        }
        
        public PokeTypes PokeType
        {
            get;
            set;
        }

        public PokeSpecies PokeSpecies
        {
            get;
            set;
        }

        public void Attack()
        {
            switch (PokeType)
            {
                case PokeTypes.Grass:
                    //green
                    Console.ForegroundColor = ConsoleColor.Green;
                    break;
                case PokeTypes.Fire:
                    //red
                    Console.ForegroundColor = ConsoleColor.Red;
                    break;
                case PokeTypes.Water:
                    //blue
                    Console.ForegroundColor = ConsoleColor.Blue;
                    break;
                case PokeTypes.Electric:
                    //yellow
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    break;
                
            }   
            Console.WriteLine($"{PokeSpecies.ToString().ToUpper()}!");
            Console.ResetColor();
        }
        public static void MakePokemon() 
        {
            Pokemon bulbasaur = new Pokemon();
            bulbasaur.PokeSpecies = PokeSpecies.Bulbasaur;
            bulbasaur.PokeType = PokeTypes.Grass;
            bulbasaur.Attack();
            
            Pokemon charmander = new Pokemon();
            charmander.PokeSpecies = PokeSpecies.Charmander;
            charmander.PokeType = PokeTypes.Fire;
            charmander.Attack();
            
            Pokemon squirtle = new Pokemon();
            squirtle.PokeSpecies = PokeSpecies.Squirtle;
            squirtle.PokeType = PokeTypes.Water;
            squirtle.Attack();
            
            Pokemon pikachu = new Pokemon();
            pikachu.PokeSpecies = PokeSpecies.Pikachu;
            pikachu.PokeType = PokeTypes.Electric;
            pikachu.Attack();
            
        }

        static Pokemon FirstConsciousPokemon(Pokemon[] pokemons)
        {
            for (int i = 0; i < pokemons.Length; i++)
            {
                if (pokemons[i].HP > 0)
                {
                    return pokemons[i];
                }
            }
            return null;
        }

        public static void TestConsciousPokemon()
        {
            Pokemon bulbasaur = new Pokemon();
            bulbasaur.PokeSpecies = PokeSpecies.Bulbasaur;
            bulbasaur.PokeType = PokeTypes.Grass;
            bulbasaur.HP = 0;
            
            Pokemon charmander = new Pokemon();
            charmander.PokeSpecies = PokeSpecies.Charmander;
            charmander.PokeType = PokeTypes.Fire;
            charmander.HP = 0;
            
            Pokemon squirtle = new Pokemon();
            squirtle.PokeSpecies = PokeSpecies.Squirtle;
            squirtle.PokeType = PokeTypes.Water;
            squirtle.HP = 2;
            
            Pokemon pikachu = new Pokemon();
            pikachu.PokeSpecies = PokeSpecies.Pikachu;
            pikachu.PokeType = PokeTypes.Electric;
            
            Pokemon[] pokemons = new Pokemon[4];
            pokemons[0] = bulbasaur;
            pokemons[1] = charmander;
            pokemons[2] = squirtle;
            pokemons[3] = pikachu;

            Pokemon first = FirstConsciousPokemon(pokemons);
            first.Attack();

            
        }

        public static void TestConsciousPokemonSafe()
        {
            Pokemon bulbasaur = new Pokemon();
            bulbasaur.PokeSpecies = PokeSpecies.Bulbasaur;
            bulbasaur.PokeType = PokeTypes.Grass;
            bulbasaur.HP = 0;
            
            Pokemon charmander = new Pokemon();
            charmander.PokeSpecies = PokeSpecies.Charmander;
            charmander.PokeType = PokeTypes.Fire;
            charmander.HP = 0;
            
            Pokemon squirtle = new Pokemon();
            squirtle.PokeSpecies = PokeSpecies.Squirtle;
            squirtle.PokeType = PokeTypes.Water;
            squirtle.HP = 2;
            
            Pokemon pikachu = new Pokemon();
            pikachu.PokeSpecies = PokeSpecies.Pikachu;
            pikachu.PokeType = PokeTypes.Electric;
            
            Pokemon[] pokemons = new Pokemon[4];
            pokemons[0] = bulbasaur;
            pokemons[1] = charmander;
            pokemons[2] = squirtle;
            pokemons[3] = pikachu;

            Pokemon first = FirstConsciousPokemon(pokemons);
            if (first != null)
            {
                first.Attack();
            }
            else
            {
                Console.WriteLine("Al je Pokémon zijn KO! Haast je naar het Pokémon center.");
            }
        }
        
        public static void RestoreHP(Pokemon pokemon)
        {
            pokemon.HP = pokemon.MaxHP;
        }

        public static void DemoRestoreHP()
        {
            Pokemon bulbasaur = new Pokemon();
            bulbasaur.PokeSpecies = PokeSpecies.Bulbasaur;
            bulbasaur.PokeType = PokeTypes.Grass;
            bulbasaur.HP = 0;

            Pokemon charmander = new Pokemon();
            charmander.PokeSpecies = PokeSpecies.Charmander;
            charmander.PokeType = PokeTypes.Fire;
            charmander.HP = 0;

            Pokemon squirtle = new Pokemon();
            squirtle.PokeSpecies = PokeSpecies.Squirtle;
            squirtle.PokeType = PokeTypes.Water;
            squirtle.HP =  2;

            Pokemon pikachu = new Pokemon();
            pikachu.PokeSpecies = PokeSpecies.Pikachu;
            pikachu.PokeType = PokeTypes.Electric;

            Pokemon[] pokemons = new Pokemon[4];
            pokemons[0] = bulbasaur;
            pokemons[1] = charmander;
            pokemons[2] = squirtle;
            pokemons[3] = pikachu;
// aanmaken van array bewusteloze Pokemon van 4 soorten zoals eerder: zelf doen
            for (int i = 0; i < pokemons.Length; i++)
            {
                Pokemon.RestoreHP(pokemons[i]);
            }

            for (int i = 0; i < pokemons.Length; i++)
            {
                Console.WriteLine(pokemons[i].HP);
            }
        }
    } 
}
