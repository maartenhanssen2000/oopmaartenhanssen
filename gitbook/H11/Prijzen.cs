﻿using System;
using System.Collections.Generic;

namespace OOP
{
    public class Prijzen
    {
        static double[] AskForPrices()
        {
            double[] prices;
            prices = new double[20];

            Console.WriteLine("Gelieve 20 prijzen in te geven.");
            for (int i = 0; i < prices.Length; i++)
            {
                Console.Write($"{i + 1} prijs is: ");
                prices[i] = Convert.ToDouble(Console.ReadLine());
            }

            return prices;
        }
        public static void Main()
        {
            double total = 0.0;
            foreach (var price in AskForPrices())
            {
                if (price > 5.00)
                {
                    Console.WriteLine(Math.Round(price,2));
                }
                
                total += price;
                
            }
            double average = total / 20;
            Console.WriteLine($"Het gemiddelde bedrap is {Math.Round(average,2)}.");
        }
    }
}