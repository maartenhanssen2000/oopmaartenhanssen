﻿using System;
using System.Collections.Generic;

namespace OOP
{
    public class PlayingCard
    {
        static Random r = new Random();
        public PlayingCard()
        {
            int number = r.Next(1, 13);
            switch (number)
            {
                case 11:
                    card = "Jack";
                    break;
                case 12:
                    card = "Queen";
                    break;
                case 13:
                    card = "King";
                    break;
                default:
                    card = number.ToString();
                    break;
            }
            switch (r.Next(1,4))
            {
                case 1:
                    Suite = Suites.Clubs;
                    break;
                case 2:
                    Suite = Suites.Diamonds;
                    break;
                case 3:
                    Suite = Suites.Hearts;
                    break;
                case 4:
                    Suite = Suites.Spades;
                    break;
            }
        }
        public PlayingCard(int number, int suite)
        {
            switch (number)
            {
                case 11:
                    card = "Jack";
                    break;
                case 12:
                    card = "Queen";
                    break;
                case 13:
                    card = "King";
                    break;
                default:
                    card = number.ToString();
                    break;
            }
            switch (suite)
            {
                case 1:
                    Suite = Suites.Clubs;
                    break;
                case 2:
                    Suite = Suites.Diamonds;
                    break;
                case 3:
                    Suite = Suites.Hearts;
                    break;
                case 4:
                    Suite = Suites.Spades;
                    break;
            }
        }
        public PlayingCard(int number, Suites suite)
        {
            switch (number)
            {
                case 11:
                    card = "Jack";
                    break;
                case 12:
                    card = "Queen";
                    break;
                case 13:
                    card = "King";
                    break;
                default:
                    card = number.ToString();
                    break;
            }
            Suite = suite;
        }
        

        private string card;
        private Suites Suite;

        public static List<PlayingCard> GenerateDeck()
        {
            List<PlayingCard> deck = new List<PlayingCard>();
            /*List<PlayingCard> cards = new List<PlayingCard>(13);
            foreach (var suite in Enum.GetValues(typeof(Suites)))
            {
                var typeOfCard = suite;
                foreach (var card in cards)
                {
                    cards.Add(new PlayingCard(card, typeOfCard));
                }
                deck.Add(cards);
                cards.clear();
            }*/
            for (int i = 1; i <= 4; i++)
            {
                int suite = i;
                for (int j = 1; j <= 13; j++)
                {
                    int card = j;
                    deck.Add(new PlayingCard(card,suite));
                }
            }

            return deck;
        }

        public static void ShowSuffleDeck(List<PlayingCard> cards)
        {
            int number;
            int i = 1;
            while (cards.Count > 0)
            {
                number = r.Next(0, cards.Count);
                Console.WriteLine($"{i}. {cards[number].card} {cards[number].Suite}");
                cards.RemoveAt(number);
                i++;
            }

            Console.WriteLine("Alle kaarten zijn getoond.");
        }
    }
}