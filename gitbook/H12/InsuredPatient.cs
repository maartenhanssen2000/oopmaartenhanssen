﻿using System;

namespace OOP
{
    public class InsuredPatient: Patient
    {
        public InsuredPatient(string name, uint stayingDuration) : base(name, stayingDuration)
        {
            
        }
        
        
        public override void ShowCost()
        {
            double cost = Math.Round(((50 + Convert.ToDouble(base.stayingDuration) * 20)*0.9 ),2) ;
            Console.WriteLine($"{name}, een verzekerde patient die {stayingDuration} in het ziekenhuis gelegen heeft, betaald {cost}.00 euro.");
        }
    }
}