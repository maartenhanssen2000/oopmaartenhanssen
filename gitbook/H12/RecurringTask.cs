﻿namespace OOP
{
    public class RecurringTask: Task
    {
        private byte days;
        
        public RecurringTask(string discription, byte days) : base(discription)
        {
            this.days = days;
        }
    }
}