﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Claims;
using OOProgrammeren;

namespace OOP
{
    public class H12
    {
        public static void Main()
        {
            while (true)
            {
                Console.WriteLine("Submenu Hoofdstuk 12");
                Console.WriteLine("1. DemonstrateWorkingStudent\n2. DemonstrateTasks\n3. DemonstratePattiens\n4. Main menu");

                bool succes = Int32.TryParse(Console.ReadLine(), out int choice);
                if (succes)
                {
                    switch (choice)
                    {
                        case 1:
                            Console.WriteLine($"Je hebt gekozen voor {choice}.");
                            System.Threading.Thread.Sleep(1000);
                            Console.Clear();
                            WorkingStudent.DemonstrateWorkingStudent();
                            break;
                        case 2:
                            Console.WriteLine($"Je hebt gekozen voor {choice}.");
                            System.Threading.Thread.Sleep(1000);
                            Console.Clear();
                            DemonstrateTasks();
                            break;
                        case 3:
                            Console.WriteLine($"Je hebt gekozen voor {choice}.");
                            System.Threading.Thread.Sleep(1000);
                            Console.Clear();
                            DemonstratePatiens();
                            break;
                        case 4:
                            Console.WriteLine($"Je hebt gekozen voor {choice}.");
                            System.Threading.Thread.Sleep(1000);
                            Console.Clear();
                            Program.Main();
                            break;
                        default:
                            Console.Clear();
                            Console.WriteLine("Ongeldige keuze. ");
                            System.Threading.Thread.Sleep(1000);
                            Console.Clear();
                            break;
                    }
                } else
                {
                    Console.Clear();
                    Console.WriteLine("Geef een getal in.");
                    System.Threading.Thread.Sleep(1000);
                    Console.Clear();
                }
            }
        }

        static void DemonstrateTasks()
        {
            ArrayList tasksArrayList = new ArrayList();

            Console.WriteLine("Wat wil je doen?");
            Console.WriteLine("1. een taak maken\n2. een terugkerende taak maken\n3. stoppen");
            int input = Int32.Parse(Console.ReadLine());
            switch (input)
            {
                case 1:
                    Console.WriteLine("beschrijving van de taak?");
                    string discription = Console.ReadLine();
                    tasksArrayList.Add(new Task(discription));
                    Console.WriteLine($"Taak {discription} is aangemaakt.");
                    break;
                case 2:
                    Console.WriteLine("beschrijving van de taak?");
                    discription = Console.ReadLine();
                    Console.WriteLine();
                    byte days = Byte.Parse(Console.ReadLine());
                    tasksArrayList.Add(new RecurringTask(discription,days));
                    Console.WriteLine($"Taak {discription} is aangemaak.t");
                    Console.WriteLine($"Deze taak moet om de {days} dagen herhaald worden.");
                    break;
                case 3:
                    Main();
                    break;
                default:
                    Console.WriteLine("Geef de juiste waarde in");
                    DemonstrateTasks();
                    break;
            }
        }

        static void DemonstratePatiens()
        {
            Patient Vincent = new Patient("Vincent", 12);
            InsuredPatient Tim = new InsuredPatient("tim", 12);
            Vincent.ShowCost();
            Tim.ShowCost();
        }
        
    }
}