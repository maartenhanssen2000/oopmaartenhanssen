﻿using System;

namespace OOP
{
    public class Patient
    {
        protected string name;
        protected uint stayingDuration;

        public Patient(string name, uint stayingDuration)
        {
            this.name = name;
            this.stayingDuration = stayingDuration;
        }

        public virtual void ShowCost()
        {
            uint cost = 50 + stayingDuration * 20;
            Console.WriteLine($"{name}, een gewone patient die {stayingDuration} in het ziekenhuis gelegen heeft, betaald {cost}.00 euro.");
        }
    }
}