﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace OOP
{
    public class WorkingStudent: Student
    {
        private byte workHours;

        public WorkingStudent()
        {
            workHours = 10;
        }
        
        public WorkingStudent(string name, int age, int markCommunication, int markProgrammingPrinciples, int webTech, byte workHours):base(name,age, markCommunication, markProgrammingPrinciples, webTech)
        {
            if (workHours < 1) workHours = 1;
            else if (workHours > 20) workHours = 20;
            else this.workHours = workHours;
        }
        
        public bool HasWorkToday()
        {
            Random random = new Random();
            return random.Next(2) == 1;
        }

        public static void DemonstrateWorkingStudent()
        {
            List<Student> studentsList = new List<Student>();
            studentsList.Add(new Student("Jhon Doe", 19, 15,18,16));
            studentsList.Add(new WorkingStudent("Jhon Wick", 25, 17,13,15,5));

            foreach (var student in studentsList)
            {
                student.ShowOverview();
                Console.WriteLine();
            }
        }

        public override void ShowOverview()
        {
            base.ShowOverview();
            Console.WriteLine($"Statuus: werkstudent\n Aantal werkuren per week: {workHours}");
        }
    }
}