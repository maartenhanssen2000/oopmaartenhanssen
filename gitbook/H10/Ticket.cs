﻿using System;

namespace OOP
{
    public class Ticket
    {
        private byte prize;
        public byte Prize
        {
            get => prize;
            set => prize = value;
        }

        public static void Raffle()
        {
            Random rdm = new Random();
            for (int i = 0; i < 10; i++)
            {
                Ticket ticket = new Ticket();
                ticket.Prize = Convert.ToByte(rdm.Next(1, 100));
                Console.WriteLine($"Waarde van het lotje: {ticket.Prize}");
            }
        }
    }
}