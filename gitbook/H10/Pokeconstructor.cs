﻿using System;

namespace OOP
{
    public class Pokeconstructor
    {
            private static int grassAttacks = 0;
            private static int fireAttacks = 0;
            private static int waterAttacks = 0;
            private static int ElectricAttacks = 0;
            
            private int maxHp;
            private int hp;
            private PokeSpecies pokeSpecies;
            private PokeTypes pokeType;
            
    
        public Pokeconstructor(int maxHP, PokeSpecies pokemonSpecies, PokeTypes pokemonType) : this(maxHP, maxHP/2, pokemonSpecies, pokemonType)
        { 
            hp = maxHP/2;
        }
        public Pokeconstructor(int maxHP, int HP, PokeSpecies pokemonSpecies, PokeTypes pokemonType)
        {
             maxHp = maxHP;
             hp = HP;
             pokeSpecies = pokemonSpecies;
             pokeType = pokemonType;
         }
        
       
        public Pokeconstructor()
        {
            maxHp = 20;
            hp = 20;
            Random r = new Random();
            switch (r.Next(1,5))
            {
                case 1:
                    pokeSpecies = PokeSpecies.Bulbasaur;
                    pokeType = PokeTypes.Grass;
                    break;
                case 2:
                    pokeSpecies = PokeSpecies.Charmander;
                    pokeType = PokeTypes.Fire;
                    break;
                case 3:
                    pokeSpecies = PokeSpecies.Pikachu;
                    pokeType = PokeTypes.Electric;
                    break;
                case 4:
                    pokeSpecies = PokeSpecies.Squirtle;
                    pokeType = PokeTypes.Water;
                    break;
            }
        }
               
        public void Attack()
        {
            switch (pokeType)
            {
                case PokeTypes.Grass:
                    //green
                    Console.ForegroundColor = ConsoleColor.Green;
                    grassAttacks++;
                    break;
                case PokeTypes.Fire:
                    //red
                    Console.ForegroundColor = ConsoleColor.Red;
                    fireAttacks++;
                    break;
                case PokeTypes.Water:
                    //blue
                    Console.ForegroundColor = ConsoleColor.Blue;
                    waterAttacks++;
                    break;
                case PokeTypes.Electric:
                    //yellow
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    ElectricAttacks++;
                    break;
                
            }   
            Console.WriteLine($"{pokeSpecies.ToString().ToUpper()}!");
            Console.ResetColor();
        }
        public static void MakePokemon() 
        {
            Pokeconstructor bulbasaur = new Pokeconstructor(20,20, PokeSpecies.Bulbasaur, PokeTypes.Grass);
            bulbasaur.Attack();
            Pokeconstructor charmander = new Pokeconstructor(20,20, PokeSpecies.Charmander, PokeTypes.Fire);
            charmander.Attack();
            Pokeconstructor squirtle = new Pokeconstructor(20,20, PokeSpecies.Squirtle, PokeTypes.Water);
            squirtle.Attack();
            Pokeconstructor pikachu = new Pokeconstructor(20,20, PokeSpecies.Pikachu, PokeTypes.Electric);
            pikachu.Attack();
        }
        public static void ConstructPokemonChained() 
        {
            Pokeconstructor bulbasaur = new Pokeconstructor(20, PokeSpecies.Bulbasaur, PokeTypes.Grass);
            bulbasaur.Attack();
            Pokeconstructor charmander = new Pokeconstructor(20, PokeSpecies.Charmander, PokeTypes.Fire);
            charmander.Attack();
            Pokeconstructor squirtle = new Pokeconstructor(20, PokeSpecies.Squirtle, PokeTypes.Water);
            squirtle.Attack();
            Pokeconstructor pikachu = new Pokeconstructor(20, PokeSpecies.Pikachu, PokeTypes.Electric);
            pikachu.Attack();
        }
        public static void DemonstrateCounter() 
                {
                    Pokeconstructor bulbasaur = new Pokeconstructor(20, PokeSpecies.Bulbasaur, PokeTypes.Grass);
                    bulbasaur.Attack();
                    bulbasaur.Attack();              
                    bulbasaur.Attack();              
                    bulbasaur.Attack();              
                    bulbasaur.Attack();              
                    Pokeconstructor charmander = new Pokeconstructor(20, PokeSpecies.Charmander, PokeTypes.Fire);
                    charmander.Attack();
                    charmander.Attack();
                    charmander.Attack();
                    charmander.Attack();
                    charmander.Attack();
                    charmander.Attack();
                    Pokeconstructor squirtle = new Pokeconstructor(20, PokeSpecies.Squirtle, PokeTypes.Water);
                    squirtle.Attack();
                    squirtle.Attack();
                    squirtle.Attack();
                    squirtle.Attack();
                    squirtle.Attack();
                    Pokeconstructor pikachu = new Pokeconstructor(20, PokeSpecies.Pikachu, PokeTypes.Electric);
                    pikachu.Attack();
                    pikachu.Attack();
                    pikachu.Attack();
                    pikachu.Attack();
                    pikachu.Attack();
                    Pokeconstructor bulbasaur2 = new Pokeconstructor(20, PokeSpecies.Bulbasaur, PokeTypes.Grass);
                    bulbasaur2.Attack();
                    bulbasaur2.Attack();
                    bulbasaur2.Attack();
                    bulbasaur2.Attack();
                    bulbasaur2.Attack();
                    bulbasaur2.Attack();
                    bulbasaur2.Attack();
                    Console.WriteLine($"Aantal aanvallen van Pokemon type `Grass` : {grassAttacks}");
                    Console.WriteLine($"Aantal aanvallen van Pokemon type `Fire` : {fireAttacks}");
                    Console.WriteLine($"Aantal aanvallen van Pokemon type `Electric` : {ElectricAttacks}");
                    Console.WriteLine($"Aantal aanvallen van Pokemon type `Water` : {waterAttacks}");
                   
                }
       
    }
}