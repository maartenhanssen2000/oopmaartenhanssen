﻿using System;

namespace OOP
{
    public class H10
    {
        public static void Main()
        {
            while (true)
            {
                Console.WriteLine("Submenu Hoofdstuk 10");
                Console.WriteLine("1. Pokemons makkelijk annmaken (H10-pokeconstructie)\n2. Pokemons nog makkelijker aanmaken (H10-chaining)\n3. Globale statistieken bijhouden (H10-pokebattlecount)\n4. Gemeenschappelijke kenmerken (H10-tombola)\n5. CVS\n6. Main menu");

                bool succes = Int32.TryParse(Console.ReadLine(), out int choice);
                if (succes)
                {
                    switch (choice)
                    {
                        case 1:
                            Console.WriteLine($"Je hebt gekozen voor {choice}.");
                            System.Threading.Thread.Sleep(1000);
                            Console.Clear();
                            Pokeconstructor.MakePokemon();
                            break;
                        case 2:
                            Console.WriteLine($"Je hebt gekozen voor {choice}.");
                            System.Threading.Thread.Sleep(1000);
                            Console.Clear();
                            Pokeconstructor.ConstructPokemonChained();
                            break;
                        case 3:
                            Console.WriteLine($"Je hebt gekozen voor {choice}.");
                            System.Threading.Thread.Sleep(1000);
                            Console.Clear();
                            Pokeconstructor.DemonstrateCounter();
                            break;
                        case 4:
                            Console.WriteLine($"Je hebt gekozen voor {choice}.");
                            System.Threading.Thread.Sleep(1000);
                            Console.Clear();
                            Ticket.Raffle();
                            break;
                        case 5:
                            Console.WriteLine($"Je hebt gekozen voor {choice}.");
                            System.Threading.Thread.Sleep(1000);
                            Console.Clear();
                            CSVDemo.Run();
                            break;
                        case 6:
                            Console.WriteLine($"Je hebt gekozen voor {choice}.");
                            System.Threading.Thread.Sleep(1000);
                            Console.Clear();
                            Program.Main();
                            break;
                        default:
                            Console.Clear();
                            Console.WriteLine("Ongeldige keuze. ");
                            System.Threading.Thread.Sleep(1000);
                            Console.Clear();
                            break;
                    }
                } else
                {
                    Console.Clear();
                    Console.WriteLine("Geef een getal in.");
                    System.Threading.Thread.Sleep(1000);
                    Console.Clear();
                }
            }
        }
    }
}