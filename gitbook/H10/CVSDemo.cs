﻿using System;
using System.Net;

namespace OOP
{
    public static class CSVDemo
    {
        public static void Run()
        {
            //024
            WebClient wc = new WebClient();
            string csv = wc.DownloadString("http://samplecsvs.s3.amazonaws.com/SalesJan2009.csv");
            csv.Trim();
            string[] split = csv.Split(',');

            int counter = 0;
            for (int i = 1; i < split.Length; i++)
            {
                string[] lijnsplit = split[i].Split('\n');
                if (i % 1 == 0 || i % 2 == 0 || i % 3 == 0)
                {
                    Console.Write($"{lijnsplit[0]}\t");
                }
                if (counter==3)
                {
                    Console.WriteLine("\n");
                    i += 8;
                    counter = 0;
                }
                counter++;
            }
        }
    }
}
