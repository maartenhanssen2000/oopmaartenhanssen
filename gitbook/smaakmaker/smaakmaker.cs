﻿using System;

namespace OOP
{
    public class Smaakmaker
    {
        public static void Main()
        {
            while (true)
            {
                Console.WriteLine("Submenu smaakmakeer");
                Console.WriteLine("1. Vormen tekenen (h8-vormen)\n2. Auto's laten rijden (h8-autos)\n3. Main menu");

                bool succes = Int32.TryParse(Console.ReadLine(), out int choice);
                if (succes)
                {
                    switch (choice)
                    {
                        case 1:
                            Console.WriteLine($"Je hebt gekozen voor {choice}.");
                            System.Threading.Thread.Sleep(1000);
                            Console.Clear();
                            DrawingShapes();
                            break;
                        case 2:
                            Console.WriteLine($"Je hebt gekozen voor {choice}.");
                            System.Threading.Thread.Sleep(1000);
                            Console.Clear(); 
                            CarDriving();
                            break;   
                        case 3:
                            Console.WriteLine($"Je hebt gekozen voor {choice}.");
                            System.Threading.Thread.Sleep(1000);
                            Console.Clear();
                            Program.Main();
                            break;
                        default:
                            Console.Clear();
                            Console.WriteLine("Ongeldige keuze. ");
                            System.Threading.Thread.Sleep(1000);
                            Console.Clear();
                            break;
                    }
                } else
                {
                    Console.Clear();
                    Console.WriteLine("Geef een getal in.");
                    System.Threading.Thread.Sleep(1000);
                    Console.Clear();
                }
            }
        }
        static void DrawingShapes()
        {
            ShapesBuilder builder = new ShapesBuilder();
            builder.Color = ConsoleColor.Green;
            builder.Symbol = '*';
            string line = builder.Line(10);
            Console.WriteLine(line);
            builder.Color = ConsoleColor.Red;
            string rectangle = builder.Rectangle(10, 5);
            Console.WriteLine(rectangle);
            builder.Color = ConsoleColor.Blue;
            string triangle = builder.Triangle(10);
            Console.WriteLine(triangle);
            
        }

        static void CarDriving()
        {
            Car car1 = new Car();
            for (int i = 0; i < 5; i++)
            {
                car1.Gas();
            }
            for (int i = 0; i < 3; i++)
            {
                car1.Brake();
            }
            Car car2 = new Car();
            for (int i = 0; i < 10; i++)
            {
                car2.Gas();
            }

            for (int i = 0; i < 1; i++)
            {
                car2.Brake();
            }
            Console.WriteLine($"Auto 1 : {car1.Speed:f2}km/h, afgelgde weg {car1.Odometer:f2}km");
            Console.WriteLine($"Auto 2 : {car2.Speed:f2}km/h, afgelgde weg {car2.Odometer:f2}km");
        }
    }
}