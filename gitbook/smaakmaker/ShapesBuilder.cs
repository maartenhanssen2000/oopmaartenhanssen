﻿using System;

namespace OOP
{
    class ShapesBuilder
    {
        private ConsoleColor color;
        public ConsoleColor Color
        {
            get { return color; }
            set
            {
                color= value;
                Console.ForegroundColor = color;
            }
        }

        private char symbol;

        public char Symbol
        {
            get { return symbol;}
            set { symbol = value;}
        }

        public string Line(int length)
        {
            return Line(length, symbol);
        }
        public string Line(int length, char alternateSymbol)
        {
            return new string(alternateSymbol, length);
        }

        public string Rectangle (int width, int heigth)
        {
            return Rectangle(width, heigth, symbol);
        }
        
        public string Triangle(int heigth)
        {
            return Triangle(heigth, symbol);
        }
        
        public string Rectangle (int width, int heigth, char alternateSymbol)
        {
            string output = "";
            if (width == 1 && heigth == 1)
            {
                output += "\u025E3";
            }
            else
            {
                for (int i = 0; i < heigth; i++)
                {
                    output += Line(width, alternateSymbol);
                    if (i != heigth-1)
                    {
                        output += "\n";
                    }
                } 
            }

            return output;

        }
        
        public string Triangle(int heigth, char alternateSymbol)
        {
            string output = "";
            if (heigth == 1)
            {
                output += "\u025E3";
            }
            else
            {
                for (int i = 0; i < heigth; i++)
                {
                    output += Line(i+1, alternateSymbol);
                    if (i < heigth - 1)
                    {
                        output += "\n";
                    }
                } 
            }

            return output;

        }
        
    }
}